from setuptools import setup

__version__ = '0.1'

setup(name='otherpack',
      version=__version__,
      description='otherpack',
      url='http://gitlab.com/thesage21/otherpack',
      author='Arjoonn Sharma',
      author_email='arjoonn.94@gmail.com',
      install_requires=['requests'],
      packages=['otherpack'],
      keywords=['otherpack'],
      zip_safe=False)
